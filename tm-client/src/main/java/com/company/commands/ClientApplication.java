package com.company.commands;


import com.company.commands.util.Bootstrap;

public class ClientApplication {
    public static void main(String[] args) throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.initClasses();
        bootstrap.start();
    }

}
