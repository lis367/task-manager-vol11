package com.company.commands.taskCommands;


import com.company.commands.DataCommands.AbstractCommand;
import com.company.commands.ServiceLocator;
import com.company.commands.TerminalServiceImpl;
import com.company.endpoint.Task;
import com.company.endpoint.TaskEndPointService;
import com.company.exception.EmptyField;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.xml.datatype.XMLGregorianCalendar;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


@NoArgsConstructor

public final class TaskListCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-list";
    }

    @Override
    public String description() {
        return "Show all tasks";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final TaskEndPointService taskServiceImpl = serviceLocator.getTaskEndPointService();
        @NotNull
        final TerminalServiceImpl terminalService = serviceLocator.getTerminalService();
        System.out.println("[TASK LIST]");

        List<Task> tasksPool = taskServiceImpl.getTaskEndPointPort().taskList(serviceLocator.getSession(), serviceLocator.getUser().getUserId());
        System.out.println("How do you want to sort? Press 1 - by creation date, Press 2 - by date begin, " +
                "Press 3 - by date end, Press 4 - by status");
        System.out.println("Press 5 - if you want to search by name or description");
        String line = terminalService.nextLine();
        if(line.isEmpty()){
            throw new EmptyField();
        }
        switch (line){
            case "1":{
                //Вывод в порядке создания
                Collections.sort(tasksPool, Comparator.comparing(Task::getCreationDate, XMLGregorianCalendar::compare));
                break;
            }
            case"2":{
                //Вывод по даче начала
                Collections.sort(tasksPool, Comparator.comparing(Task::getDateBegin, XMLGregorianCalendar::compare));
                break;
            }
            case"3":{
                //Вывод по дате окончания
                Collections.sort(tasksPool, Comparator.comparing(Task::getDateEnd,XMLGregorianCalendar::compare));
                break;
            }
            case "4":{
                //Вывод по статусу
                Collections.sort(tasksPool, Comparator.comparing(Task::getDisplayName));
                break;
            }
            case "5":{
                {
                    System.out.println("PLEASE ENTER NAME OR DERSCRIPTION");
                    line= terminalService.nextLine();
                    tasksPool= searchByString(line, tasksPool);
                    break;
                }
            }
        }
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-YYYY");

        for(int i=0;i<tasksPool.size();i++){
            System.out.println("Task Name: "+tasksPool.get(i).getName() + " Task ID: " + tasksPool.get(i).getId()
                    + " Task description is: "+tasksPool.get(i).getDescription()
                    + " Task date start: "+ tasksPool.get(i).getDateBegin()
                    + " Task date end: " + tasksPool.get(i).getDateEnd()
                    + " Task status is : "+tasksPool.get(i).getDisplayName() + " Project :"+tasksPool.get(i).getProjectID());
        }
    }

    public List searchByString(String name, List <Task> projectPool){
        List<Task> arrayWithName = new ArrayList();
        for(Task array: projectPool){
            if(array.getName().contains(name)||array.getDescription().contains(name)){
                arrayWithName.add(array);
            }
        }

        return arrayWithName;
    }

    public boolean secureCommand() {
        return true;
    }

    public TaskListCommand(ServiceLocator bootstrap) { setServiceLocator(bootstrap);
    }

}
