package com.company.commands.taskCommands;


import com.company.commands.DataCommands.AbstractCommand;
import com.company.commands.ServiceLocator;
import com.company.endpoint.TaskEndPointService;
import com.company.exception.NoPermission;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import java.util.Scanner;
@NoArgsConstructor

public final class TaskViewCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-view";
    }

    @Override
    public String description() {
        return "Date and name view of project";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final TaskEndPointService taskServiceImpl = serviceLocator.getTaskEndPointService();

        System.out.println("ENTER ID");
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        try{
            if (taskServiceImpl.getTaskEndPointPort().taskRead(serviceLocator.getSession(), line).getUserId().equals(serviceLocator.getUser().getUserId())){
                System.out.println("Task name "+ taskServiceImpl.getTaskEndPointPort().taskRead(serviceLocator.getSession(), line).getName());
                System.out.println("Task begins in "+ taskServiceImpl.getTaskEndPointPort().taskRead(serviceLocator.getSession(), line).getDateBegin());
                System.out.println("Task ends in "+ taskServiceImpl.getTaskEndPointPort().taskRead(serviceLocator.getSession(), line).getDateEnd());
                System.out.println("Task status: "+ taskServiceImpl.getTaskEndPointPort().taskRead(serviceLocator.getSession(), line).getDisplayName());
            }
            else {
               throw new NoPermission();
            }
        }
        catch (NullPointerException npe){
           npe.printStackTrace();
        }
    }

    @Override
    public boolean secureCommand() {
        return true;
    }

    public TaskViewCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }
}
