package com.company.commands.taskCommands;


import com.company.commands.DataCommands.AbstractCommand;
import com.company.commands.ServiceLocator;
import com.company.commands.TerminalServiceImpl;
import com.company.endpoint.TaskEndPointService;
import com.company.exception.EmptyField;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

@NoArgsConstructor


public final class TaskCreateCommand extends AbstractCommand {


    @Override
    public String command() {
        return "task-create";
    }

    @Override
    public String description() {
        return "Create new task";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final TaskEndPointService taskServiceImpl = serviceLocator.getTaskEndPointService();
        final TerminalServiceImpl terminalService = serviceLocator.getTerminalService();
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        String name = terminalService.nextLine().trim();
        if(name.isEmpty()){
           throw new EmptyField();

        }
        System.out.println("ENTER PROJECT ID:");
        final String projectID = terminalService.nextLine().trim();
        System.out.println("ENTER THE TIME OF THE BEGINNING OF THE TASK");
        final String dateStart = terminalService.nextLine().trim();
        System.out.println("ENTER THE END TIME OF THE TASK");
        final String dateEnd = terminalService.nextLine().trim();
        if(!serviceLocator.isValidDate(dateStart)|!serviceLocator.isValidDate(dateEnd)){
            //throw new WrongDateFormat();
            System.out.println("WRONG DATE FORMAT");
            return;
        }
        System.out.println("ENTER THE DESCRIPTION");
        final String description = terminalService.nextLine().trim();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
        Date dateEnd2 = dateFormatter.parse(dateEnd);
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(dateEnd2);
        XMLGregorianCalendar xmlDateEnd = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        Date dateStart2 = dateFormatter.parse(dateStart);
        cal.setTime(dateStart2);
        XMLGregorianCalendar xmlDateStart = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        System.out.println("TASK ID IS");
        System.out.println(taskServiceImpl.getTaskEndPointPort().taskCreate(serviceLocator.getSession(), name , projectID, xmlDateStart , xmlDateEnd, serviceLocator.getUser().getUserId(), description) );
    }
    public boolean secureCommand() {
        return true;
    }

    public TaskCreateCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }
}
