package com.company.commands.DataCommands;


import com.company.endpoint.ProjectEndPointService;
import com.company.endpoint.TaskEndPointService;
import com.company.endpoint.UserEndPointService;
import com.company.endpoint.UserRoleType;
import com.company.exception.NoPermission;

public class SaveCommand extends AbstractCommand{
    @Override
    public String command() {
        return "save-command";
    }

    @Override
    public String description() {
        return "Save Users, Projects, Tasks";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator.getUser().getUserRoleType().equals(UserRoleType.ADMIN)){
        final UserEndPointService userServiceImpl = serviceLocator.getUserEndPointService();
        final ProjectEndPointService projectServiceInterface = serviceLocator.getProjectEndPointService();
        final TaskEndPointService taskServiceInterface = serviceLocator.getTaskEndPointService();


        userServiceImpl.getUserEndPointPort().saveInDataBase(serviceLocator.getSession());
        projectServiceInterface.getProjectEndPointPort().saveInDB(serviceLocator.getSession());
        taskServiceInterface.getTaskEndPointPort().saveInDB(serviceLocator.getSession());

        System.out.println("SUCCESS");}
        else throw new NoPermission();
    }

    @Override
    public boolean secureCommand() {
        return true;
    }
}
