package com.company.commands.DataCommands;

import com.company.endpoint.Data;
import com.company.endpoint.DataEndPointService;
import com.company.endpoint.UserRoleType;
import com.company.exception.NoPermission;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;

import java.io.File;


public class JaksonXMLLoad extends AbstractCommand{
    @Override
    public String command() {
        return "jaksonxml-load";
    }

    @Override
    public String description() {
        return "jaksonxml-load";
    }

    @SneakyThrows
    @Override
    public void execute() throws Exception {
        if (serviceLocator.getUser().getUserRoleType().equals(UserRoleType.ADMIN)){
        final DataEndPointService dataService = serviceLocator.getDataEndPointService();
        Data data;
        XmlMapper objectMapper = new XmlMapper();
        File workingFolder = new File("tm-server/src/main/resources/Jaxb");
        File dataFile = new File(workingFolder,"JAKSONXML.xml");
        data = objectMapper.readValue(dataFile, Data.class);
        dataService.getDataEndPointPort().load(serviceLocator.getSession(), data);
        System.out.println("SUCCESS");}
        else throw new NoPermission();

    }

    @Override
    public boolean secureCommand() {
        return false;
    }
}
