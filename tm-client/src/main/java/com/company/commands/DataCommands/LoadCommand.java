package com.company.commands.DataCommands;


import com.company.endpoint.ProjectEndPointService;
import com.company.endpoint.TaskEndPointService;
import com.company.endpoint.UserEndPointService;
import com.company.endpoint.UserRoleType;
import com.company.exception.NoPermission;
import org.jetbrains.annotations.NotNull;
import lombok.NoArgsConstructor;
@NoArgsConstructor

public class LoadCommand extends AbstractCommand {
    @Override
    public String command() {
        return "load-command";
    }

    @Override
    public String description() {
        return "Load Users, Projects, Task from DB";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator.getUser().getUserRoleType().equals(UserRoleType.ADMIN)) {
            @NotNull final UserEndPointService userServiceImpl = serviceLocator.getUserEndPointService();
            final ProjectEndPointService projectServiceInterface = serviceLocator.getProjectEndPointService();
            final TaskEndPointService taskServiceInterface = serviceLocator.getTaskEndPointService();

            userServiceImpl.getUserEndPointPort().userLoad(serviceLocator.getSession());
            System.out.println("База пользователей успешно загружена");
            projectServiceInterface.getProjectEndPointPort().projectLoad(serviceLocator.getSession());
            System.out.println("База проектов успешно загружена");
            taskServiceInterface.getTaskEndPointPort().taskLoad(serviceLocator.getSession());
            System.out.println("База задач успешно загружена");

        } else throw new NoPermission();
    }


    @Override
    public boolean secureCommand() {
        return false;
    }
}
