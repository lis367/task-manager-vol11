package com.company.commands.DataCommands;

import com.company.endpoint.Data;
import com.company.endpoint.DataEndPointService;
import com.company.endpoint.ObjectFactory;
import com.company.endpoint.UserRoleType;
import com.company.exception.NoPermission;
import lombok.SneakyThrows;


import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class JaxbXMLLoadCommand extends AbstractCommand{
    @Override
    public String command() {
        return "jaxbxml-load";
    }

    @Override
    public String description() {
        return "Load JaXb in XML";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator.getUser().getUserRoleType().equals(UserRoleType.ADMIN)){
        final DataEndPointService dataService = serviceLocator.getDataEndPointService();
        final String repo_XML = "tm-server/src/main/resources/Jaxb/JAXBXml.xml";
        System.setProperty("javax.xml.bind.JAXBContext", "com.sun.xml.internal.bind.v2.ContextFactory");

        /*
        JAXBContext context = JAXBContext.newInstance(Data.class);
        Unmarshaller um = context.createUnmarshaller();
        Data data = (Data) um.unmarshal(new InputStreamReader(new FileInputStream(repo_XML), StandardCharsets.UTF_8));
        System.setProperty("javax.xml.bind.JAXBContext", "com.sun.xml.internal.bind.v2.ContextFactory");

         */
        JAXBContext context = JAXBContext.newInstance(Data.class);
        Unmarshaller um = context.createUnmarshaller();
        JAXBElement<Data> root = um.unmarshal(new StreamSource(new File(repo_XML)),Data.class);
        Data data = root.getValue();

        /*InputStream stream = new FileInputStream(repo_XML);
        JAXBContext jaxbContext = JAXBContext.newInstance(Data.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLEventReader someSource = factory.createXMLEventReader(stream);
        JAXBElement<Data> userElement = jaxbUnmarshaller.unmarshal(someSource, Data.class);
        Data data = userElement.getValue();
         */


        dataService.getDataEndPointPort().load(serviceLocator.getSession(), data);
        System.out.println("Success");}
        else throw new NoPermission();
    }

    @Override
    public boolean secureCommand() {
        return false;
    }
}
