package com.company.commands.DataCommands;


import com.company.endpoint.Data;
import com.company.endpoint.DataEndPointService;
import com.company.endpoint.ObjectFactory;
import com.company.endpoint.UserRoleType;
import com.company.exception.NoPermission;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import java.io.File;

public class JaxbXMLSaveCommand extends AbstractCommand{
    @Override
    public String command() {
        return "jaxbxml-save";
    }

    @Override
    public String description() {
        return "Save in JaXb in XML";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator.getUser().getUserRoleType().equals(UserRoleType.ADMIN)){
        final DataEndPointService dataService = serviceLocator.getDataEndPointService();
        final String jaxbXML = "tm-server/src/main/resources/Jaxb/JAXBXml.xml";
        ObjectFactory myRootFactory = new ObjectFactory();
        Data myRootType = new Data();
        myRootType = dataService.getDataEndPointPort().save(serviceLocator.getSession(), myRootType);
        System.setProperty("javax.xml.bind.JAXBContext", "com.sun.xml.internal.bind.v2.ContextFactory");
        try {


            JAXBContext context = JAXBContext.newInstance(Data.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(new JAXBElement<Data>(new QName("http://endpoint.company.com/","DataEndPointService"), Data.class,myRootType), new File(jaxbXML));
            //marshaller.marshal(myRootType,new File(jaxbXML));

            /*
            File file = new File(jaxbXML);
            JAXBContext jaxbContext = JAXBContext.newInstance(Data.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            //JAXBElement<Data> myRootElement = myRootFactory.createData(myRootType);
            jaxbMarshaller.marshal(myRootType, file);
            //jaxbMarshaller.marshal(myRootElement, System.out);


             */


        } catch (JAXBException e) {
            e.printStackTrace();
        }
        System.out.println("SUCCESS");}
        else throw new NoPermission();
    }

    @Override
    public boolean secureCommand() {
        return false;
    }
}
