package com.company.commands.DataCommands;

import com.company.endpoint.Data;
import com.company.endpoint.DataEndPointService;
import com.company.endpoint.UserRoleType;
import com.company.exception.NoPermission;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;

public class JaksonJSONLoad extends AbstractCommand{
    @Override
    public String command() {
        return "jaksonjson-load";
    }

    @Override
    public String description() {
        return "jaksonjson-load";
    }



    @Override
    public void execute() throws Exception {
        final String jaksonJSON = "tm-server/src/main/resources/Jaxb/JAKSONJson.json";
        final DataEndPointService dataService = serviceLocator.getDataEndPointService();
        if (serviceLocator.getUser().getUserRoleType().equals(UserRoleType.ADMIN)){
        ObjectMapper mapper = new ObjectMapper();
            Data data = mapper.readValue(new File(jaksonJSON),Data.class);
            dataService.getDataEndPointPort().load(serviceLocator.getSession(), data);
        System.out.println("SUCCESS");}
        else {
            throw new NoPermission();
        }
    }

    @Override
    public boolean secureCommand() {
        return false;
    }
}
