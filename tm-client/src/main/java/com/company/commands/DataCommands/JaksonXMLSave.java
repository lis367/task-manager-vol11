package com.company.commands.DataCommands;


import com.company.endpoint.Data;
import com.company.endpoint.DataEndPointService;
import com.company.endpoint.UserRoleType;
import com.company.exception.NoPermission;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

public class JaksonXMLSave extends AbstractCommand{
    @Override
    public String command() {
        return "jaksonxml-save";
    }

    @Override
    public String description() {
        return "jaksonxml-save";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator.getUser().getUserRoleType().equals(UserRoleType.ADMIN)){
        final DataEndPointService dataService = serviceLocator.getDataEndPointService();
        Data data = new Data();
        data = dataService.getDataEndPointPort().save(serviceLocator.getSession(), data);
        XmlMapper objectMapper = new XmlMapper();
        objectMapper.configure(ToXmlGenerator.Feature.WRITE_XML_1_1, true);
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
        final String xml = objectWriter.writeValueAsString(data);
        final byte[] data2 = xml.getBytes(StandardCharsets.UTF_8);
        File workingFolder = new File("tm-server/src/main/resources/Jaxb");
        File dataFile = new File(workingFolder,"JAKSONXML.xml");
        Files.write(dataFile.toPath(),data2);
        System.out.println("SUCCESS");}
        else throw new NoPermission();
    }

    @Override
    public boolean secureCommand() {
        return false;
    }
}
