package com.company.commands.DataCommands;


import com.company.endpoint.Data;
import com.company.endpoint.DataEndPointService;
import com.company.endpoint.UserRoleType;
import com.company.exception.NoPermission;
import org.eclipse.persistence.jaxb.MarshallerProperties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;

public class JaxbJSONSaveCommand extends AbstractCommand{
    @Override
    public String command() {
        return "jaxbjson-save";
    }

    @Override
    public String description() {
        return "jaxbjson-save";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator.getUser().getUserRoleType().equals(UserRoleType.ADMIN)){
        final String jaxbJSON = "tm-server/src/main/resources/Jaxb/JAXBJson.json";
        try {
            final DataEndPointService dataService = serviceLocator.getDataEndPointService();
            Data data = new Data();
            data = dataService.getDataEndPointPort().save(serviceLocator.getSession(), data);
            System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
            JAXBContext context = JAXBContext.newInstance(Data.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(MarshallerProperties.MEDIA_TYPE,
                    "application/json");
            marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(data,new File(jaxbJSON));
            System.out.println("SUCCESS");
        } catch (JAXBException e) {
            e.printStackTrace();
        }}
        else throw new NoPermission();
    }

    @Override
    public boolean secureCommand() {
        return false;
    }
}
