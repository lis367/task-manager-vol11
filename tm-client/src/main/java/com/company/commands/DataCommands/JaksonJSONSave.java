package com.company.commands.DataCommands;


import com.company.endpoint.Data;
import com.company.endpoint.DataEndPointService;
import com.company.endpoint.UserRoleType;
import com.company.exception.NoPermission;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.File;
import java.io.IOException;

public class JaksonJSONSave extends AbstractCommand{
    @Override
    public String command() {
        return "jaksonjson-save";
    }

    @Override
    public String description() {
        return "jaksonjson-save";
    }

    @Override
    public void execute() throws Exception {
        final String jaksonJSON = "tm-server/src/main/resources/Jaxb/JAKSONJson.json";
        final DataEndPointService dataServiceInterface = serviceLocator.getDataEndPointService();
        if (serviceLocator.getUser().getUserRoleType().equals(UserRoleType.ADMIN)){
        Data data = new Data();
        data = dataServiceInterface.getDataEndPointPort().save(serviceLocator.getSession(), data);
        ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
        try {
            mapper.writeValue(new File(jaksonJSON), data);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("SUCCESS");
        }else {
            throw new NoPermission();
        }
        }

    @Override
    public boolean secureCommand() {
        return false;
    }
}
