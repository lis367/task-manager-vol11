package com.company.commands.DataCommands;


import com.company.endpoint.Data;
import com.company.endpoint.DataEndPointService;
import com.company.endpoint.UserRoleType;
import com.company.exception.NoPermission;
import lombok.SneakyThrows;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.transform.stream.StreamSource;
import java.io.*;

public class JaxbJSONLoadCommand extends AbstractCommand{
    @Override
    public String command() {
        return "jaxbjson-load";
    }

    @Override
    public String description() {
        return "jaxbjson-load";
    }

    @SneakyThrows
    @Override
    public void execute() throws Exception {
        if (serviceLocator.getUser().getUserRoleType().equals(UserRoleType.ADMIN)){
        final DataEndPointService dataService = serviceLocator.getDataEndPointService();
        try {
            final String jaxbJSON = "tm-server/src/main/resources/Jaxb/JAXBJson.json";
            System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");

            JAXBContext context = JAXBContext.newInstance(Data.class);
            Unmarshaller um = context.createUnmarshaller();
            um.setProperty(UnmarshallerProperties.MEDIA_TYPE,
                    "application/json");
            um.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
            //JAXBElement<Data> userElement = (JAXBElement<Data>) um.unmarshal(new InputStreamReader(new FileInputStream(jaxbJSON)));
            //Data data = (Data) um.unmarshal(new InputStreamReader(new FileInputStream(jaxbJSON)));
            JAXBElement<Data> root = um.unmarshal(new StreamSource(new File(jaxbJSON)),Data.class);
            Data data = root.getValue();

            dataService.getDataEndPointPort().load(serviceLocator.getSession(), data);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        System.out.println("SUCCESS");}
        else throw new NoPermission();
    }

    @Override
    public boolean secureCommand() {
        return false;
    }
}
