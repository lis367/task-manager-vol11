package com.company.commands.userCommands;

import com.company.commands.DataCommands.AbstractCommand;
import com.company.commands.ServiceLocator;
import com.company.endpoint.UserEndPointService;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
@NoArgsConstructor

public final class UserLoadFromDBCommand extends AbstractCommand {

    @Override
    public String command() {
        return "user-load";
    }

    @Override
    public String description() {
        return "Load Users from file in Repository";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final UserEndPointService userServiceImpl = serviceLocator.getUserEndPointService();
        userServiceImpl.getUserEndPointPort().userLoad(serviceLocator.getSession());
        System.out.println("База пользователей успешно загружена");
    }
    public boolean secureCommand() {
        return false;
    }

    public UserLoadFromDBCommand(ServiceLocator serviceLocator) {
        setServiceLocator(serviceLocator);
    }

}
