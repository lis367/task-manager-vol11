package com.company.commands.userCommands;


import com.company.commands.DataCommands.AbstractCommand;
import com.company.commands.ServiceLocator;
import com.company.commands.TerminalServiceImpl;
import com.company.endpoint.User;
import com.company.endpoint.UserEndPointService;
import com.company.exception.NoPermission;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
@NoArgsConstructor

public final class UserDeleteCommand extends AbstractCommand {

    @Override
    public String command() {
        return "user-delete";
    }

    @Override
    public String description() {
        return "Delete user from repository";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final UserEndPointService userServiceImpl = serviceLocator.getUserEndPointService();
        final TerminalServiceImpl terminalService = serviceLocator.getTerminalService();

        System.out.println("ENTER LOGIN");

        final String login = terminalService.nextLine();
        System.out.println("ENTER PASSWORD");

        final String password = userServiceImpl.getUserEndPointPort().generateMD5(terminalService.nextLine());
        final User user = userServiceImpl.getUserEndPointPort().userRead(login,password);

        if(user.getUserId().equals(serviceLocator.getUser().getUserId())){
            userServiceImpl.getUserEndPointPort().userRemove(serviceLocator.getSession(), user);
            System.out.println("Profile deleted successfully");
        }
        else{
           throw new NoPermission();

        }
    }

    @Override
    public boolean secureCommand() {
        return true;
    }

    public UserDeleteCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }
}
