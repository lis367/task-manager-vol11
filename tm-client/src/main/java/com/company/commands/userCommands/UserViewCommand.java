package com.company.commands.userCommands;

import com.company.commands.DataCommands.AbstractCommand;
import com.company.commands.ServiceLocator;
import lombok.NoArgsConstructor;

@NoArgsConstructor

public final class UserViewCommand extends AbstractCommand {

    @Override
    public String command() {
        return "user-view";
    }

    @Override
    public String description() {
        return "User view";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("User name: "+serviceLocator.getUser().getName());
        System.out.println("User id: "+serviceLocator.getUser().getUserId());
        System.out.println("User roletype: "+serviceLocator.getUser().getUserRoleType());
        System.out.println("User password: "+serviceLocator.getUser().getPassword());
    }

    @Override
    public boolean secureCommand() {
        return true;
    }

    public UserViewCommand(ServiceLocator bootstrap) {
      setServiceLocator(bootstrap);
    }


}
