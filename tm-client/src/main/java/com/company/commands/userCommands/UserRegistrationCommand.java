package com.company.commands.userCommands;

import com.company.commands.DataCommands.AbstractCommand;
import com.company.commands.ServiceLocator;
import com.company.commands.TerminalServiceImpl;
import com.company.endpoint.User;
import com.company.endpoint.UserEndPointService;
import com.company.endpoint.UserRoleType;
import com.company.exception.EmptyField;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@NoArgsConstructor

public final class UserRegistrationCommand extends AbstractCommand {

    @Override
    public String command() {
        return "user-registration";
    }

    @Override
    public String description() {
        return "Registration";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final UserEndPointService userServiceImpl = serviceLocator.getUserEndPointService();
        final TerminalServiceImpl terminalService = serviceLocator.getTerminalService();

        System.out.println("ENTER LOGIN");
        final String login = terminalService.nextLine();
        if(login.isEmpty()){
            throw new EmptyField();
        }
        System.out.println("ENTER PASSWORD");
        String password = terminalService.nextLine();
        if(password.isEmpty()){
            throw new EmptyField();
        }
        password = userServiceImpl.getUserEndPointPort().generateMD5(password);
        final User user = new User();
        user.setName(login);
        user.setPassword(password);
        final String id =  UUID.randomUUID().toString();
        user.setUserId(id);
        user.setUserRoleType(UserRoleType.USER);
        userServiceImpl.getUserEndPointPort().userSave(serviceLocator.getSession(), user);
        System.out.println("User id = "+ id + " with login "+login +" password "+password);
    }

    public boolean secureCommand() {
        return false;
    }

    public UserRegistrationCommand(ServiceLocator serviceLocator) {
        setServiceLocator(serviceLocator);
    }


}
