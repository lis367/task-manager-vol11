package com.company.commands.userCommands;

import com.company.commands.DataCommands.AbstractCommand;
import com.company.commands.ServiceLocator;
import com.company.commands.TerminalServiceImpl;
import com.company.endpoint.User;
import com.company.endpoint.UserEndPointService;
import com.company.exception.EmptyField;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
@NoArgsConstructor

public final class UserUpdateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "user-update";
    }

    @Override
    public String description() {
        return "Update User";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final UserEndPointService userServiceImpl = serviceLocator.getUserEndPointService();
        final TerminalServiceImpl terminalService = serviceLocator.getTerminalService();

        System.out.println("ENTER NEW LOGIN");
        final String login = terminalService.nextLine();
        if (login.isEmpty()){
            throw new EmptyField();
        }
        System.out.println("ENTER NEW PASSWORD");
        String password = terminalService.nextLine();
        if(password.isEmpty()){
            throw  new EmptyField();
        }
        password = userServiceImpl.getUserEndPointPort().generateMD5(password);
        final User user = serviceLocator.getUser();
        user.setName(login);
        user.setPassword(password);
        serviceLocator.setUser(user);
        userServiceImpl.getUserEndPointPort().userUpdate(serviceLocator.getSession(), user);
        System.out.println("Success");
    }

    public boolean secureCommand() {
        return true;
    }

    public UserUpdateCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }


}
