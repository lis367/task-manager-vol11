package com.company.commands.userCommands;


import com.company.commands.DataCommands.AbstractCommand;
import com.company.commands.ServiceLocator;
import com.company.endpoint.SessionEndPointService;
import com.company.endpoint.UserEndPointService;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
@NoArgsConstructor

public final class UserEndSessionCommand extends AbstractCommand {
    @Override
    public String command() {
        return "user-end";
    }

    @Override
    public String description() {
        return "End of User session";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final SessionEndPointService sessionservice = serviceLocator.getSessionEndPointService();
        sessionservice.getSessionEndPointPort().closeSession(serviceLocator.getSession());
        System.out.println("LOG OUT SUCCESSFUL");
        serviceLocator.setUser(null);
    }
    public boolean secureCommand() {
        return true;
    }

    public UserEndSessionCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }
}
