package com.company.commands.util;

import com.company.commands.DataCommands.AbstractCommand;
import com.company.commands.ServiceLocator;
import com.company.commands.TerminalServiceImpl;
import com.company.endpoint.*;
import com.company.exception.CommandCorruptException;
import lombok.SneakyThrows;
import org.reflections.Reflections;
import java.io.Serializable;
import java.lang.Exception;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public final class Bootstrap implements ServiceLocator {

    private final ProjectEndPointService projectEndPointService = new ProjectEndPointService();
    private final TaskEndPointService taskEndPointService = new TaskEndPointService();
    private final UserEndPointService userEndPointService = new UserEndPointService();
    private final TerminalServiceImpl terminalService = new TerminalServiceImpl();
    private final DataEndPointService dataEndPointService = new DataEndPointService();
    private final SessionEndPointService sessionEndPointService = new SessionEndPointService();

    private final Map<String, AbstractCommand> commands =
            new LinkedHashMap<>();

    private User user;
    private Session session;



    public void registry(final AbstractCommand command) throws CommandCorruptException {
        final String cliCommand = command.command();
        final String cliDescription = command.description();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandCorruptException();
        commands.put(cliCommand, command);
    }


    public void start() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!"exit".equals(command)) {
            command = scanner.nextLine();
            execute(command);
            System.out.println();
        }

    }

    @SneakyThrows
    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) {
            return;}
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) {
            return; }
        if ((user==null)&&(abstractCommand.secureCommand())){
            System.out.println("SECURE COMMAND. YOU NEED TO LOG IN");
        }
        else{
            try{
                abstractCommand.execute();
            }
            catch (Exception e){
                e.printStackTrace();}
        }

    }

    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void initClasses() throws CommandCorruptException {
        final Set<Class<? extends AbstractCommand>> classes =
                new Reflections("com.company").getSubTypesOf(AbstractCommand.class);

        AbstractCommand abstractCommand = null;

        for (Class<? extends AbstractCommand> aClass : classes){
            try {
                abstractCommand = aClass.newInstance();
                abstractCommand.setServiceLocator(this);
                registry(abstractCommand);
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

            /*
            try {
                /*
                Class clazz = Class.forName(AbstractCommand.class.getName());
                Class[] params = {Bootstrap.class};

                Constructor[] constructors = clazz.getConstructors();
                for (Constructor constructor : constructors) {
                    Class[] paramTypes = constructor.getParameterTypes();
                    for (Class paramType : paramTypes) {
                        System.out.print(paramType.getName() + " ");
                    }
                    System.out.println();
                }
                abstractCommand = (AbstractCommand)clazz.getConstructor(params).CLASSES[i].newInstance(this);

                //
                abstractCommand = (AbstractCommand)CLASSES[i].newInstance();
                if(abstractCommand instanceof AbstractCommand){
                abstractCommand.setServiceLocator(this);
                registry(abstractCommand);
                }
                else return;

            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }

            }

             */

    }
    public boolean isValidDate(String inDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(inDate.trim());
        } catch (ParseException pe) {
            return false;
        }
        return true;
    }

    @Override
    public void setSession(Session session) {
        this.session = session;
    }

    @Override
    public Session getSession() {
        return session;
    }

    public ProjectEndPointService getProjectEndPointService() {
        return projectEndPointService;
    }
    @Override
    public TaskEndPointService getTaskEndPointService() {
        return taskEndPointService;
    }
    @Override
    public UserEndPointService getUserEndPointService() {
        return userEndPointService;
    }
    @Override
    public TerminalServiceImpl getTerminalService() {
        return terminalService;
    }
    @Override
    public DataEndPointService getDataEndPointService() {
        return dataEndPointService;
    }
    @Override
    public SessionEndPointService getSessionEndPointService() {
        return sessionEndPointService;
    }
}
