package com.company.commands.projectCommands;

import com.company.commands.DataCommands.AbstractCommand;
import com.company.commands.ServiceLocator;
import com.company.commands.TerminalServiceImpl;
import com.company.endpoint.Project;
import com.company.endpoint.ProjectEndPointService;
import com.company.exception.EmptyField;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.xml.datatype.XMLGregorianCalendar;
import java.text.SimpleDateFormat;
import java.util.*;

@NoArgsConstructor

public final class ProjectListCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-list";
    }
    @Override
    public String description() {
        return "Show all projects.";
    }
    @Override
    public void execute() throws Exception {
        @NotNull
        final ProjectEndPointService projectService = serviceLocator.getProjectEndPointService();
        final TerminalServiceImpl terminalService = new TerminalServiceImpl();

        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-YYYY");
        System.out.println("[PROJECT LIST]");

        List <Project> projectPool = projectService.getProjectEndPointPort().projectList(serviceLocator.getSession(),serviceLocator.getUser().getUserId());
        System.out.println("How do you want to sort? Press 1 - by creation date, Press 2 - by date begin, " +
                "Press 3 - by date end, Press 4 - by status");
        System.out.println("Press 5 - if you want to search by name or description");
        String line = terminalService.nextLine();
        if(line.isEmpty()){
            throw new EmptyField();
        }
        switch (line){
            case "1":{
                //Вывод в порядке создания
                Collections.sort(projectPool, Comparator.comparing(Project::getCreationDate, XMLGregorianCalendar::compare));

                break;
            }
            case"2":{
                //Вывод по даче начала
                Collections.sort(projectPool, Comparator.comparing(Project::getDateBegin, XMLGregorianCalendar::compare));
                break;
            }
            case"3":{
                //Вывод по дате окончания
                Collections.sort(projectPool, Comparator.comparing(Project::getDateEnd,XMLGregorianCalendar::compare));
                break;
            }
            case "4":{
                //Вывод по статусу
                Collections.sort(projectPool, Comparator.comparing(Project::getDisplayName));
                break;
            }
            case "5":{
                System.out.println("PLEASE ENTER NAME OR DERSCRIPTION");
                line= terminalService.nextLine();
                projectPool = searchByString(line, projectPool);
                break;
            }

        }

        for(int i=0; i<projectPool.size();i++){
            System.out.println("Project Name: "+projectPool.get(i).getName() + " Project ID: " + projectPool.get(i).getId()
                    + " Project description: "+projectPool.get(i).getDescription()
                    + " Project date start: "+projectPool.get(i).getDateBegin()
                    + " Project date end: "+projectPool.get(i).getDateEnd()
                    + " Project status is: "+projectPool.get(i).getDisplayName());
         }

    }
    public boolean secureCommand() {
        return true;
    }

    public List searchByString(String name, List <Project> projectPool){
        List<Project> arrayWithName = new ArrayList();
        for(Project array: projectPool){
            if(array.getName().contains(name)||array.getDescription().contains(name)){
                arrayWithName.add(array);
            }
        }

        return arrayWithName;
    }


    public ProjectListCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }
}
