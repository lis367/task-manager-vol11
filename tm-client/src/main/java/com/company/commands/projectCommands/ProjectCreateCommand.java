package com.company.commands.projectCommands;

import com.company.commands.ServiceLocator;
import com.company.commands.DataCommands.AbstractCommand;
import com.company.commands.TerminalServiceImpl;
import com.company.endpoint.EmptyField;
import com.company.endpoint.ProjectEndPointService;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

@NoArgsConstructor

public final class ProjectCreateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-create";
    }

    @Override
    public String description() {
        return "Create new project.";
    }

    @SneakyThrows
    @Override
    public void execute() throws Exception {
        @NotNull
        final ProjectEndPointService projectService = serviceLocator.getProjectEndPointService();
        @NotNull
        final TerminalServiceImpl terminalService = new TerminalServiceImpl();
        System.out.println("ENTER NAME:");
        @Nullable
        String name = terminalService.nextLine().trim();
        if (name.isEmpty()) {
           //throw new EmptyField();
            System.out.println("EMPTY FIELD");
            return;
        }
        System.out.println("ENTER THE TIME OF THE BEGINNING OF THE PROJECT");
        @Nullable
        final String dateStart = terminalService.nextLine().trim();
        System.out.println("ENTER THE END TIME OF THE PROJECT");
        @Nullable
        final String dateEnd = terminalService.nextLine().trim();
        if(!serviceLocator.isValidDate(dateStart)|!serviceLocator.isValidDate(dateEnd)){
            System.out.println("WRONG DATE FORMAT dd.MM.yyyy");
            return;
        }
        System.out.println("ENTER THE DESCRIPTION OF THE PROJECT");
        final String description = terminalService.nextLine().trim();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
        Date dateEnd2 = dateFormatter.parse(dateEnd);
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(dateEnd2);
        XMLGregorianCalendar xmlDateEnd = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        Date dateStart2 = dateFormatter.parse(dateStart);
        cal.setTime(dateStart2);
        XMLGregorianCalendar xmlDateStart = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        System.out.println("YOUR PROJECT ID IS");
        System.out.println(projectService.getProjectEndPointPort().projectCreate(serviceLocator.getSession(),
                name, xmlDateStart,xmlDateEnd, serviceLocator.getUser().getUserId(),description));

    }
    public boolean secureCommand() {
        return true;
    }

    public ProjectCreateCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }


}
