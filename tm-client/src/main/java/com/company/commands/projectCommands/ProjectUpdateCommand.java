package com.company.commands.projectCommands;


import com.company.commands.DataCommands.AbstractCommand;
import com.company.commands.ServiceLocator;
import com.company.commands.TerminalServiceImpl;
import com.company.endpoint.ObjectIsNotFound;
import com.company.endpoint.Project;
import com.company.endpoint.ProjectEndPointService;
import com.company.endpoint.Status;
import com.company.exception.EmptyField;
import com.company.exception.NoPermission;
import com.company.exception.WrongDateFormat;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;


import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

@NoArgsConstructor

public class ProjectUpdateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-update";
    }

    @Override
    public String description() {
        return "Update project";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final ProjectEndPointService projectService = serviceLocator.getProjectEndPointService();
        final TerminalServiceImpl terminalService = serviceLocator.getTerminalService();
        System.out.println("ENTER ID");
        String line = terminalService.nextLine();
        try {
            if (projectService.getProjectEndPointPort().read(serviceLocator.getSession(), line).getUserId().equals(serviceLocator.getUser().getUserId())) {
                final Project projectPool = projectService.getProjectEndPointPort().read(serviceLocator.getSession(), line);
                System.out.println("ENTER NEW NAME");
                line = terminalService.nextLine();
                if(line.isEmpty()){
                    System.out.println("EMPTY FIELD");
                    throw new EmptyField();
                }
                projectPool.setName(line);
                System.out.println("ENTER THE TIME OF THE BEGINNING OF THE PROJECT");
                final String dateStart = terminalService.nextLine().trim();
                System.out.println("ENTER THE END TIME OF THE PROJECT");
                final String dateEnd = terminalService.nextLine().trim();
                if(!serviceLocator.isValidDate(dateStart)|!serviceLocator.isValidDate(dateEnd)){
                    throw new WrongDateFormat();
                }
                SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
                Date dateEnd2 = dateFormatter.parse(dateEnd);
                GregorianCalendar cal = new GregorianCalendar();
                cal.setTime(dateEnd2);
                XMLGregorianCalendar xmlDateEnd = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
                Date dateStart2 = dateFormatter.parse(dateStart);
                cal.setTime(dateStart2);
                XMLGregorianCalendar xmlDateStart = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
                projectPool.setDateBegin(xmlDateStart);
                projectPool.setDateEnd(xmlDateEnd);
                System.out.println("ENTER THE STATUS OF THE PROJECT");
                System.out.println("1 - Pending, 2 - Running, 3 - Finished");
                line = terminalService.nextLine();
                if(line.isEmpty()){
                    throw new EmptyField();
                }
                switch (line){
                    case "1":
                        projectPool.setDisplayName(Status.PENDING);
                        break;
                    case "2":
                        projectPool.setDisplayName(Status.RUNNING);
                        break;
                    case "3":
                        projectPool.setDisplayName(Status.FINISHED);
                        break;
                }
                projectService.getProjectEndPointPort().update(serviceLocator.getSession(), projectPool);
                System.out.println("SUCCESS");
            } else {
                throw new NoPermission();

            }
        }
        catch (NullPointerException e){
           e.printStackTrace();
        }
    }

    @Override
    public boolean secureCommand() {
        return true;
    }

    public ProjectUpdateCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }
}
