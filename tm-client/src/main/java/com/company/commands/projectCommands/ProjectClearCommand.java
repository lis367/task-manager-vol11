package com.company.commands.projectCommands;

import com.company.commands.DataCommands.AbstractCommand;
import com.company.commands.ServiceLocator;
import com.company.endpoint.ProjectEndPointService;
import com.company.endpoint.TaskEndPointService;
import com.company.endpoint.UserRoleType;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.naming.NoPermissionException;

@NoArgsConstructor

public final class ProjectClearCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "Remove all projects";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final ProjectEndPointService projectService = serviceLocator.getProjectEndPointService();
        @NotNull
        final TaskEndPointService taskServiceImpl = serviceLocator.getTaskEndPointService();
        if (serviceLocator.getUser().getUserRoleType().equals(UserRoleType.ADMIN)){
        projectService.getProjectEndPointPort().projectClear(serviceLocator.getSession());
        taskServiceImpl.getTaskEndPointPort().taskClear(serviceLocator.getSession());
        System.out.println("ALL PROJECT&TASK REMOVED");}
        else {
            throw new NoPermissionException();
        }
    }

    public ProjectClearCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }
    public boolean secureCommand() {
        return true;
    }




}
