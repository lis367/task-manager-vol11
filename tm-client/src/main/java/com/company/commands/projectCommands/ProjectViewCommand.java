package com.company.commands.projectCommands;

import com.company.commands.DataCommands.AbstractCommand;
import com.company.commands.ServiceLocator;
import com.company.commands.TerminalServiceImpl;
import com.company.endpoint.ObjectIsNotFound;
import com.company.endpoint.ProjectEndPointService;
import com.company.exception.NoPermission;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;


import java.text.SimpleDateFormat;

@NoArgsConstructor

public class ProjectViewCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-view";
    }

    @Override
    public String description() {
        return "Date and name view of project";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final ProjectEndPointService projectService = serviceLocator.getProjectEndPointService();
        final TerminalServiceImpl terminalService = serviceLocator.getTerminalService();
        System.out.println("ENTER ID");
        String line = terminalService.nextLine();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-YYYY");
        try{
            if (projectService.getProjectEndPointPort().read(serviceLocator.getSession(), line).getUserId().equals(serviceLocator.getUser().getUserId())){
                System.out.println("Project name "+ projectService.getProjectEndPointPort().read(serviceLocator.getSession(), line).getName());
                System.out.println("Project begins in "+ simpleDateFormat.format(projectService.getProjectEndPointPort().read(serviceLocator.getSession(), line).getDateBegin()));
                System.out.println("Project ends in "+simpleDateFormat.format(projectService.getProjectEndPointPort().read(serviceLocator.getSession(), line).getDateEnd()));
                System.out.println("Project status is "+(projectService.getProjectEndPointPort().read(serviceLocator.getSession(), line).getDisplayName()));
            }
            else {
               throw new NoPermission();
            }
        }
        catch (NullPointerException npe){
           npe.printStackTrace();
        }
    }

    @Override
    public boolean secureCommand() {
        return true;
    }

    public ProjectViewCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }
}
