package com.company.commands.projectCommands;


import com.company.commands.DataCommands.AbstractCommand;
import com.company.commands.ServiceLocator;
import com.company.commands.TerminalServiceImpl;
import com.company.endpoint.ObjectIsNotFound;
import com.company.endpoint.ProjectEndPointService;
import com.company.exception.NoPermission;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor


public final class ProjectRemoveCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-remove";
    }

    @Override
    public String description() {
        return "Remove selected project.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final ProjectEndPointService projectService = serviceLocator.getProjectEndPointService();
        final TerminalServiceImpl terminalService = serviceLocator.getTerminalService();
        System.out.println("ENTER ID");

        String line = terminalService.nextLine();

            if (projectService.getProjectEndPointPort().read(serviceLocator.getSession(), line).getUserId().equals(serviceLocator.getUser().getUserId())) {
                projectService.getProjectEndPointPort().projectRemove(serviceLocator.getSession(), line);
                System.out.println("PROJECT&TASK REMOVED");
            } else {
                throw new NoPermission();
            }


    }
    public boolean secureCommand() {
        return true;
    }

    public ProjectRemoveCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);

    }

}
