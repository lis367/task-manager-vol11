package com.company.exception;

public class ObjectIsAlreadyExist extends Exception {

    @Override
    public String toString() {
        return "Объект с таким ID уже существует";
    }
}
