package com.company.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sun.istack.Nullable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)

public class Data implements Serializable {

    @Nullable
    private List<Project> projects = new ArrayList<>();
    @Nullable
    private List<Task> tasks = new ArrayList<>();
    @Nullable
    private List<User> users = new ArrayList<>();


}
