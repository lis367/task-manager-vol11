package com.company.entity;

import com.company.service.ProjectServiceImpl;
import com.company.service.*;

public final class InitialService {
    public Object lookup (String name) {
        if (name.equalsIgnoreCase("Project-service")) {
            return new ProjectServiceImpl();
        } else if (name.equalsIgnoreCase("Task-service")) {
            return new TaskServiceImpl();
        } else if (name.equalsIgnoreCase("User-service")) {
            return new UserServiceImpl();
        } else if (name.equalsIgnoreCase("Terminal-service")) {
            return new TerminalServiceImpl();
        } else if (name.equalsIgnoreCase("Data-service")){
            return new DataService();}
        else if (name.equalsIgnoreCase("Session-service"))
            return new SessionServiceImpl();
        return null;
    }

}
