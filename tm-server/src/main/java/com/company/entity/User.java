package com.company.entity;



import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Setter
@Getter

public final class User implements Serializable {
    @NotNull
    private String name;
    @NotNull
    private String password;
    @NotNull
    private UserRoleType userRoleType;
    @NotNull
    private String userId;
    private static final long serialVersionUID = 1L;

    public User (){

    }

    public User(String name, String password) {
        this.name = name;
        this.password = password;
    }


}
