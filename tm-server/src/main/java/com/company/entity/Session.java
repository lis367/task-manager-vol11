package com.company.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.UUID;

@NoArgsConstructor
@Setter
@Getter
public class Session implements Cloneable {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    private String userId;

    @Nullable
    private String signature;

    @Nullable
    private Long timestamp;

    @Override
    public Session clone() {
        try{return (Session) super.clone();}
        catch (CloneNotSupportedException e){
            return null;
        }
    }
}
