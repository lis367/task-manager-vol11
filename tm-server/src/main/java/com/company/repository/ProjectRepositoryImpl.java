package com.company.repository;

import com.company.Interfaces.ProjectRepoInterface;
import com.company.Interfaces.TaskRepoInterface;
import com.company.entity.Project;
import com.company.entity.Task;
import com.company.exception.ObjectIsAlreadyExist;
import com.company.exception.ObjectIsNotFound;
import lombok.Getter;
import lombok.Setter;
import java.io.*;
import java.util.*;

@Getter
@Setter

public final class ProjectRepositoryImpl implements ProjectRepoInterface {
    Map<String, Project> projectRepository;
    TaskRepoInterface taskRepository;

    public Project findOne(String id) throws ObjectIsNotFound {
        try {
            return projectRepository.get(id);
        } catch (NullPointerException e) {
            throw new ObjectIsNotFound();
        }
    }

    public List<Project> findAll(String userID) {
        List<Project> projects = new ArrayList<>();
        for (Map.Entry<String, Project> projectPool : projectRepository.entrySet()) {
            if ((projectPool.getValue().getUserId()).equals(userID)) {
                projects.add(projectPool.getValue());
            }
        }
        return projects;
    }

    @Override
    public List<Project> findAll() {
        List<Project> projects = new ArrayList<Project>(projectRepository.values());
        return projects;
    }

    public void persist(Project project) {
        Project projectPersist = projectRepository.get(project.getId());
        if (projectPersist == null) {
            projectRepository.put(project.getId(), project);
        } else try {
            throw new ObjectIsAlreadyExist();
        } catch (ObjectIsAlreadyExist objectIsAlreadyExist) {
            objectIsAlreadyExist.printStackTrace();
        }


        /* Проверка по совпадению имени?
        Project projectPersist = projectRepository.get(project.getId());
        for (Map.Entry<String,Project> projectEntry: projectRepository.entrySet()){
            if(projectEntry.getValue().getName().equals(project.getName())){
                try {
                    throw new ObjectIsAlreadyExist();
                } catch (ObjectIsAlreadyExist objectIsAlreadyExist) {
                    objectIsAlreadyExist.printStackTrace();
                }
            }
        }
        projectRepository.put(project.getId(),project);

         */
    }

    public void merge(Project project) {
        projectRepository.put(project.getId(), project);
    }

    public void remove(String id) {
        projectRepository.remove(id);
        ArrayList<Task> taskWithProject = taskRepository.getTasksFromProject(id);
        for (int i = 0; i < taskWithProject.size(); i++) {
            taskRepository.remove(taskWithProject.get(i).getId());
        }

    }

    public void removeAll() {
        projectRepository.clear();
        taskRepository.removeAll();
    }

    //Сохранение в обход класса Дата
    @Override
    public void saveInDB() throws Exception {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("tm-server/src/main/resources/projects.ser");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            ArrayList<Project> projects = new ArrayList<>(projectRepository.values());
            for (Project projectEntry : projects) {
                objectOutputStream.writeObject(projectEntry);
            }
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //Загрузка в обход класса Дата
    @Override
    public void load() throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream("tm-server/src/main/resources/projects.ser");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        boolean loop = true;
        while (loop){
            if(fileInputStream.available()!=0){
                Project project = (Project) objectInputStream.readObject();
                projectRepository.put(project.getId(), project);}
            else {
                loop = false;
            }
        }
        fileInputStream.close();
        objectInputStream.close();
    }


    public ProjectRepositoryImpl(Map<String, Project> projectRepository, TaskRepoInterface taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public ProjectRepositoryImpl() {
    }
}
