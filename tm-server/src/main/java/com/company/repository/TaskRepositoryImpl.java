package com.company.repository;

import com.company.Interfaces.TaskRepoInterface;
import com.company.entity.Task;
import com.company.exception.ObjectIsAlreadyExist;
import lombok.Getter;
import lombok.Setter;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Getter
@Setter
public final class TaskRepositoryImpl implements TaskRepoInterface {
    private Map<String, Task> taskRepository;

    public void persist(Task task)  {
        Task taskPersist = taskRepository.get(task.getId());
        if (taskPersist ==null){
            taskRepository.put(task.getId(), task);
        }
        else try {
            throw new ObjectIsAlreadyExist();
        } catch (ObjectIsAlreadyExist objectIsAlreadyExist) {
            objectIsAlreadyExist.printStackTrace();
        }
    }

    public Task findOne(String id){
        return taskRepository.get(id);
    }

    public List findAll(String userID){
       List<Task> tasks = new ArrayList<>();
        for (Map.Entry<String, Task> taskPool: taskRepository.entrySet()){
            if((taskPool.getValue().getUserId()).equals(userID)){
                tasks.add(taskPool.getValue());
            }
        }
        return tasks;
    }

    @Override
    public List<Task> findAll() {
        List<Task> tasks = new ArrayList<Task>(taskRepository.values());
        return tasks;
    }

    public void merge (Task task)
    {
        taskRepository.put(task.getId(), task);
    }

    public void remove(String id){
        taskRepository.remove(id);
    }

    public void removeAll(){
        taskRepository.clear();
    }


    public ArrayList getTasksFromProject(String projectID){
        ArrayList<Task> arrayList = new ArrayList<Task>();
        for (Map.Entry<String, Task> taskEntry : taskRepository.entrySet()) {
           if(projectID.equals(taskEntry.getValue().getProjectID())){
               arrayList.add(taskEntry.getValue());
            }
        }
        return arrayList;
    }

    @Override
    public void saveInDB() throws Exception {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("tm-server/src/main/resources/tasks.ser");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            ArrayList<Task> tasks = new ArrayList<>(taskRepository.values());
            for (Task taskEntry : tasks) {
                objectOutputStream.writeObject(taskEntry);
            }
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void load() throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream("tm-server/src/main/resources/tasks.ser");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        boolean loop = true;
        while (loop){
            if(fileInputStream.available()!=0){
                Task task = (Task) objectInputStream.readObject();
               taskRepository.put(task.getId(), task);}
            else {
                loop = false;
            }
        }
        fileInputStream.close();
        objectInputStream.close();

    }

    public TaskRepositoryImpl(Map<String, Task> taskRepository) {
        this.taskRepository = taskRepository;
    }

    public TaskRepositoryImpl() {
    }
}

