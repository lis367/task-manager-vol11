package com.company.repository;


import com.company.Interfaces.UserRepoInterface;
import com.company.entity.User;
import com.company.exception.ObjectIsAlreadyExist;
import com.company.exception.ObjectIsNotFound;
import lombok.Getter;
import lombok.Setter;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


@Getter
@Setter
public final class UserRepositoryImpl implements UserRepoInterface {
    Map<String, User> userRepository;


    public User findOne(String login, String password){
        for (Map.Entry <String, User> userEntry: userRepository.entrySet()){
            if(userEntry.getValue().getName().equals(login)&&userEntry.getValue().getPassword().equals(password)){
                return userEntry.getValue();
            }
        }
            return null;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<User>(userRepository.values());
        return users;
    }

    public void persist(User user)
    {
        User userPersist = userRepository.get(user.getUserId());
        if (userPersist ==null){
            userRepository.put(user.getUserId(),user);
        }
        else try {
            throw new ObjectIsAlreadyExist();
        } catch (ObjectIsAlreadyExist objectIsAlreadyExist) {
            objectIsAlreadyExist.printStackTrace();
        }
    }

    public void merge(User user) {
        User userUpdate = userRepository.get(user.getUserId());
        if(userUpdate ==null){
            try {
                throw new ObjectIsNotFound();
            } catch (ObjectIsNotFound objectIsNotFound) {
                objectIsNotFound.printStackTrace();
            }
        }
        else {
            userRepository.put(user.getUserId(), user);
        }
    }

    @Override
    public void remove(User user) {
        Iterator<Map.Entry<String, User>> iterator = userRepository.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, User> entry = iterator.next();
            if (user.getUserId().equals(entry.getValue().getUserId())) {
                iterator.remove();
            }
        }

    }

    public void save() throws IOException {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("tm-server/src/main/resources/user.ser");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            ArrayList<User> arrayUser = new ArrayList<>(userRepository.values());
            for (User userEntry : arrayUser) {
                objectOutputStream.writeObject(userEntry);
            }
            objectOutputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public UserRepositoryImpl(Map<String, User> userRepository) {
        this.userRepository = userRepository;
    }

    public void load() throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream("tm-server/src/main/resources/user.ser");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        boolean loop = true;
        while (loop){
            if(fileInputStream.available()!=0){
        User user = (User) objectInputStream.readObject();
        userRepository.put(user.getUserId(), user);}
        else {
            loop = false;
            }
        }
        fileInputStream.close();
        objectInputStream.close();
    }


    public UserRepositoryImpl() {
    }
}
