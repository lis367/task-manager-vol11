package com.company.repository;

import com.company.Interfaces.SessionRepoInterface;
import com.company.entity.Project;
import com.company.entity.Session;
import com.company.entity.Task;
import com.company.exception.ObjectIsAlreadyExist;
import com.company.exception.ObjectIsNotFound;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SessionRepositoryImpl implements SessionRepoInterface {

    private HashMap<String, Session> sessionHashMap = new HashMap<>();

    public Session findOne(String id) throws ObjectIsNotFound {
        try {
            return sessionHashMap.get(id);
        } catch (NullPointerException e) {
            throw new ObjectIsNotFound();
        }
    }

    public List<Session> findAll(){
        List<Session> sessions = new ArrayList<Session>(sessionHashMap.values());
        return sessions;
    }

    public void persist(Session session){
        Session session1 = sessionHashMap.get(session.getId());
        if (session1 == null) {
            sessionHashMap.put(session.getId(), session);
        } else try {
            throw new ObjectIsAlreadyExist();
        } catch (ObjectIsAlreadyExist objectIsAlreadyExist) {
            objectIsAlreadyExist.printStackTrace();
        }
    }

    public void merge(Session session) {
        sessionHashMap.put(session.getId(), session);
    }

    public void remove(String id) {
        sessionHashMap.remove(id);

    }

    public void removeAll() {
        sessionHashMap.clear();
    }

}
