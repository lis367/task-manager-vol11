package com.company.service;

import com.company.Interfaces.ProjectRepoInterface;
import com.company.Interfaces.ProjectServiceInterface;
import com.company.Interfaces.TaskRepoInterface;
import com.company.entity.Project;
import com.company.Interfaces.Service;
import com.company.entity.Status;
import com.company.exception.ObjectIsNotFound;
import com.company.util.Bootstrap;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.IOException;
import java.util.*;

@Getter
@Setter

public final class ProjectServiceImpl implements ProjectServiceInterface, Service {

    @NotNull
    private ProjectRepoInterface projectRepository;
    @NotNull
    private TaskRepoInterface taskRepository;
    @NotNull
    private Bootstrap bootstrap;

    public ProjectServiceImpl() {
    }

    public String projectCreate(final String name, final Date dateStart, final Date dateEnd, final String userId, final  String description) {
        String id = UUID.randomUUID().toString();
        Project project = new Project(name, dateStart, dateEnd, id,  userId);
        project.setDisplayName(Status.PENDING);
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(new Date());
        project.setCreationDate(new Date());
        project.setDescription(description);
        projectRepository.persist(project);
        return project.getId();
    }

    public List<Project> projectList(String id) {
        return projectRepository.findAll(id);
    }

    public List allProjectList(){
        return projectRepository.findAll();
    }

    @Override
    public void setProjects(List<Project> projects) {
        if (projects==null) {
            return;
        }
        for(Project project: projects){
            projectRepository.persist(project);
        }
    }

    @Override
    public void projectClear() {
        projectRepository.removeAll();
    }

    @Override
    public void load() throws IOException, ClassNotFoundException {
        projectRepository.load();
    }

    @Override
    public void saveInDb() {
        try {
            projectRepository.saveInDB();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void projectRemove(final String id) throws ObjectIsNotFound {
        if(projectRepository.findOne(id)==null){
                throw new ObjectIsNotFound();
        }
        else {
            projectRepository.remove(id);
            ArrayList<String>copyofTask = taskRepository.getTasksFromProject(id);
            for(int i=0;i<copyofTask.size();i++){
                taskRepository.remove(copyofTask.get(i));
            }
        }
    }

    public Project read(final String id) throws ObjectIsNotFound {
            return projectRepository.findOne(id);
    }

    public void update(final Project project){
        projectRepository.merge(project);
    }

    public String getName() {
        return "Project-Service";
    }







}
