package com.company.service;
import com.company.Interfaces.Service;
import com.company.Interfaces.UserRepoInterface;
import com.company.Interfaces.UserServiceInterface;
import com.company.entity.PasswordHashUtil;
import com.company.entity.User;
import com.company.entity.UserRoleType;
import com.company.exception.EmptyField;
import com.company.exception.ObjectIsNotFound;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.UUID;

public final class UserServiceImpl implements UserServiceInterface, Service{

    private UserRepoInterface userRepositoryImpl;

public void registration(String login, String password) throws Exception {

    User user = new User(login,password);
    String id =  UUID.randomUUID().toString();
    user.setUserId(id);
    user.setUserRoleType(UserRoleType.USER);
    userRepositoryImpl.persist(user);
    System.out.println("User id = "+ id + " with login "+login +" password "+password);
}

public void update(User user) throws ObjectIsNotFound, IOException {
  userRepositoryImpl.merge(user);
  userRepositoryImpl.save();
}


public String generateMD5 (String password) throws NoSuchAlgorithmException {
    PasswordHashUtil passwordHashUtil = new PasswordHashUtil();
    passwordHashUtil.md5(password);

    /*
    MessageDigest m= MessageDigest.getInstance("MD5");
    m.update(password.getBytes());
    byte[] digest = m.digest();
    StringBuilder builder = new StringBuilder();
    for (byte b : digest) {
        builder.append(String.format("%02x", b & 0xff));
    }

     */
    return passwordHashUtil.md5(password);
}

    @Override
    public void setUsers(List<User> users) {
        if(users==null) return;
        for(User user: users){
            userRepositoryImpl.persist(user);
        }
    }

    @Override
    public List allUserList() {
        return userRepositoryImpl.findAll();
    }

    public User read(String login, String password){
    return userRepositoryImpl.findOne(login,password);
}

public void save (User user) throws IOException {
    userRepositoryImpl.persist(user);
}
public void load() throws IOException, ClassNotFoundException {
    userRepositoryImpl.load();
}

public void remove(User user){
    userRepositoryImpl.remove(user);

}

    public void setUserRepository(UserRepoInterface userRepositoryImpl) {
        this.userRepositoryImpl = userRepositoryImpl;
    }

    @Override
    public void saveInDb() {
        try {
            userRepositoryImpl.save();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getName() {
        return "User-service";
    }


}
