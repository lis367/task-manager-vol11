package com.company.service;

import com.company.Interfaces.ProjectServiceInterface;
import com.company.Interfaces.Service;
import com.company.Interfaces.TaskRepoInterface;
import com.company.Interfaces.TaskServiceInterface;
import com.company.entity.Status;
import com.company.entity.Task;
import com.company.exception.EmptyField;
import com.company.exception.ObjectIsNotFound;
import com.company.util.Bootstrap;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter

public final class TaskServiceImpl implements TaskServiceInterface, Service {

    @NotNull
    private TaskRepoInterface taskRepository;
    @NotNull
    private ProjectServiceInterface projectService;
    @NotNull
    private Bootstrap bootstrap;


    public String taskCreate(String name, String projectId, Date dateStart, Date dateEnd, String userId, String description) throws Exception {
        if (projectService.read(projectId)==null){
           throw new ObjectIsNotFound();
        }
        if(!projectService.read(projectId).getUserId().equals(userId)){
            throw new ObjectIsNotFound();
        }
        String id = UUID.randomUUID().toString();
        Task task = new Task(name, id, userId);
        task.setDateBegin(dateStart);
        task.setDateEnd(dateEnd);
        task.setProjectID(projectId);
        task.setDisplayName(Status.PENDING);
        task.setCreationDate(new Date());
        task.setDescription(description);
        taskRepository.persist(task);
        return task.getId();

    }

    public Task read (String id){
        return taskRepository.findOne(id);
    }

    public void update(Task task){
        taskRepository.merge(task);
    }

    public void taskRemove(String id) throws ObjectIsNotFound {
        if(taskRepository.findOne(id)==null){
                throw new ObjectIsNotFound();
        }
        else {
            taskRepository.remove(id);
        }
    }

    public List<Task> taskList(String id) {
        return taskRepository.findAll(id);
    }

    public void taskClear() {
        taskRepository.removeAll();
    }

    @Override
    public void setTasks(List<Task> tasks) {
        if(tasks==null) return;
        for (Task task: tasks){
            taskRepository.persist(task);
        }
    }

    @Override
    public List allTaskList() {
        return taskRepository.findAll();
    }

    @Override
    public void load() throws IOException, ClassNotFoundException {
        taskRepository.load();
    }

    @Override
    public void saveInDb() {
        try {
            taskRepository.saveInDB();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getName() {
        return "Task-service";
    }
}
