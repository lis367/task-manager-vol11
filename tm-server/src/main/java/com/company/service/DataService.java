package com.company.service;

import com.company.Interfaces.*;
import com.company.entity.Data;
import com.company.exception.EmptyField;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;


public class DataService implements DataServiceInterface,Service {


    @NotNull
    private ServiceLocator serviceLocator;


    @Override
    public void load(@Nullable Data data) {
        final ProjectServiceInterface projectService = (ProjectServiceInterface) serviceLocator.getService("Project-Service");
        final TaskServiceInterface taskService = (TaskServiceInterface) serviceLocator.getService("Task-service");
        final UserServiceInterface userService = (UserServiceInterface) serviceLocator.getService("User-service");
        try{
        if(data.getProjects()==null){
            projectService.setProjects(new ArrayList<>());
        } else projectService.setProjects(data.getProjects());

        if(data.getTasks()==null){
            taskService.setTasks(new ArrayList<>());
        } else taskService.setTasks(data.getTasks());

        if(data.getUsers()==null){
            userService.setUsers(new ArrayList<>());
        } else userService.setUsers(data.getUsers());}
        catch (EmptyField e){
            e.printStackTrace();
        }



    }

    @Override
    public @Nullable Data save(@Nullable final Data data) {
        if (data == null) return data;
        final ProjectServiceInterface projectService = (ProjectServiceInterface) serviceLocator.getService("Project-service");
        final TaskServiceInterface taskService = (TaskServiceInterface) serviceLocator.getService("Task-service");
        final UserServiceInterface userService = (UserServiceInterface) serviceLocator.getService("User-service");

        data.setProjects(projectService.allProjectList());
        data.setTasks(taskService.allTaskList());
        data.setUsers(userService.allUserList());
        return data;
    }

    @Override
    public String getName() {
        return "Data-service";
    }

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }
}
