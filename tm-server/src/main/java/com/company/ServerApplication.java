package com.company;

import com.company.util.Bootstrap;

import javax.jws.WebService;

@WebService()
public class ServerApplication {
    public static void main(String[] args) throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }

}
