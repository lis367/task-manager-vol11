package com.company.endpoint;

import com.company.Interfaces.SessionServiceInterface;
import com.company.entity.Session;
import com.company.entity.User;
import com.company.exception.EmptyField;
import lombok.NoArgsConstructor;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public class SessionEndPoint {

    private SessionServiceInterface sessionService;


    @WebMethod
    public void closeSession(@WebParam(name = "Session") Session session) throws Exception {
        sessionService.closeSession(session);
    }

    @WebMethod
    public List getListSession(){
        return sessionService.getListSession();
    }

    @WebMethod
    public User getUser(){
        return sessionService.getUser();
    }

    @WebMethod
    public Session openSession(@WebParam(name = "User") User user){
        return sessionService.openSession(user);
    }

    public SessionEndPoint(SessionServiceInterface sessionService) {
        this.sessionService = sessionService;
    }
}
