package com.company.endpoint;

import com.company.Interfaces.SessionServiceInterface;
import com.company.Interfaces.UserServiceInterface;
import com.company.entity.Session;
import com.company.entity.User;
import com.company.exception.EmptyField;
import com.company.exception.ObjectIsNotFound;
import lombok.NoArgsConstructor;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@WebService
@NoArgsConstructor
public class UserEndPoint {

    private UserServiceInterface userService;
    private SessionServiceInterface sessionService;

    public UserEndPoint(UserServiceInterface userService, SessionServiceInterface sessionService) {
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @WebMethod
    public void registration(@WebParam(name = "login")final String login,
                             @WebParam(name = "password") final String password) throws Exception {
        userService.registration(login,password);
    }

    @WebMethod
    public void userUpdate(@WebParam(name = "Session") Session session,
            @WebParam(name ="User") User user) throws Exception {
        sessionService.validate(session);
        userService.update(user);
    }

    @WebMethod
    public String generateMD5(@WebParam(name = "password") String password) throws Exception {
        return userService.generateMD5(password);
    }

    @WebMethod
    public void setUsers(@WebParam(name = "Session") Session session,
                         @WebParam(name = "Users") List<User> users) throws Exception{
        sessionService.validate(session);
        userService.setUsers(users);
    }

    @WebMethod
    public List allUserList(@WebParam(name = "Session") Session session) throws Exception {
        sessionService.validate(session);
        return userService.allUserList();
    }


    @WebMethod
    public User userRead(@WebParam(name ="login") final String login,
                         @WebParam(name ="password") final String password) throws Exception {
        return userService.read(login,password);
    }

    @WebMethod
    public void userSave(@WebParam(name = "Session") Session session,@WebParam(name = "User") User user) throws Exception {
        sessionService.validate(session);
        userService.save(user);
    }

    @WebMethod
    public void userLoad(@WebParam(name = "Session") Session session) throws Exception {
        sessionService.validate(session);
        userService.load();
    }

    @WebMethod
    public void userRemove(@WebParam(name = "Session") Session session, @WebParam(name = "User") User user) throws Exception {
        sessionService.validate(session);
        userService.remove(user);
    }

    @WebMethod
    public void saveInDataBase(@WebParam(name = "Session") Session session) throws Exception {
        sessionService.validate(session);
        userService.saveInDb();
    }

}
