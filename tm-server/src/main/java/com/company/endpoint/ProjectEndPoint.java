package com.company.endpoint;

import com.company.Interfaces.ProjectServiceInterface;
import com.company.Interfaces.SessionServiceInterface;
import com.company.entity.Project;
import com.company.entity.Session;
import com.company.exception.EmptyField;
import com.company.exception.ObjectIsNotFound;
import lombok.NoArgsConstructor;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@WebService
@NoArgsConstructor
public class ProjectEndPoint {

    private ProjectServiceInterface projectService;
    private SessionServiceInterface sessionService;

    public ProjectEndPoint(ProjectServiceInterface projectService, SessionServiceInterface sessionService) {
        this.projectService = projectService;
        this.sessionService = sessionService;
    }

    @WebMethod
    public String projectCreate(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "name") final String name,
            @WebParam (name ="dateStart")final Date dateStart,
            @WebParam (name ="dateEnd")final Date dateEnd,
            @WebParam (name ="userId")final String userId,
            @WebParam (name ="description")final  String description
            ) throws Exception {
        sessionService.validate(session);
        return projectService.projectCreate(name,dateStart,dateEnd,userId,description);
    }

    @WebMethod
    public List<Project> projectList(@WebParam(name = "session")Session session, @WebParam(name = "id") String id) throws Exception {
        sessionService.validate(session);
        return projectService.projectList(id);
    }

    @WebMethod
    public void projectRemove(@WebParam(name = "Session") Session session, @WebParam(name ="id") final String id) throws Exception {
        sessionService.validate(session);
        projectService.projectRemove(id);
    }

    @WebMethod
    public Project read(@WebParam(name = "Session") Session session,@WebParam (name ="Id")final String id) throws Exception {
        sessionService.validate(session);
        return projectService.read(id);
    }

    @WebMethod
    public void update(@WebParam(name = "Session") Session session,@WebParam (name = "Project") final Project project) throws Exception {
        sessionService.validate(session);
        projectService.update(project);
    }

    @WebMethod
    public void projectClear(@WebParam(name = "Session") Session session) throws Exception {
        sessionService.validate(session);
        projectService.projectClear();
    }

    @WebMethod
    public List<Project> allProjectsList(@WebParam(name = "Session") Session session) throws Exception {
        sessionService.validate(session);
        return projectService.allProjectList();
    }

    @WebMethod
    public void projectLoad(@WebParam(name = "Session") Session session) throws Exception {
        sessionService.validate(session);
        try {
            projectService.load();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    @WebMethod
    public void saveInDB(@WebParam(name = "Session") Session session) throws Exception {
        sessionService.validate(session);
        projectService.saveInDb();
    }


}
