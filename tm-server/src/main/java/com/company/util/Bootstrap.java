package com.company.util;

import com.company.Interfaces.*;
import com.company.endpoint.*;
import com.company.entity.*;
import com.company.repository.*;

import javax.xml.ws.Endpoint;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public final class Bootstrap implements ServiceLocator {


    private final ServiceCache cache = new ServiceCache();

    private User user;

    @Override
    public Service getService(String name) {
        Service service = cache.getService(name);
        if(service!=null){
            return service;
        }
        InitialService initialService = new InitialService();
        Service service1 = (Service) initialService.lookup(name);
        cache.addService(service1);
        return service1;
    }

    public void start() throws Exception {
        serviceAndRepoInit();
        System.out.println("*** SERVER IS RUNNING ***");


    }
    // Созданы Проджект,Таск,Юзер сервисы/репозитории
    public void serviceAndRepoInit(){

        final ProjectServiceInterface projectService = (ProjectServiceInterface) getService("Project-Service");
        final TaskServiceInterface taskService = (TaskServiceInterface) getService("Task-service");
        final DataServiceInterface dataService = (DataServiceInterface) getService("Data-service");
        final SessionServiceInterface sessionService = (SessionServiceInterface) getService("Session-service");
        final Map<String, Project> projectRepositoryMap = new HashMap<>();
        final Map<String, Task> taskRepositoryMap = new HashMap<>();
        final TaskRepoInterface taskRepository = new TaskRepositoryImpl(taskRepositoryMap);
        final ProjectRepoInterface projectRepository = new ProjectRepositoryImpl(projectRepositoryMap,taskRepository);

        projectService.setProjectRepository(projectRepository);
        projectService.setTaskRepository(taskRepository);
        projectService.setBootstrap(this);
        taskService.setTaskRepository(taskRepository);
        taskService.setProjectService(projectService);
        taskService.setBootstrap(this);
        dataService.setServiceLocator(this);
        sessionService.setServiceLocator(this);

        final Map<String,User> userRepositoryMap = new HashMap<>();
        final UserRepoInterface userRepository = new UserRepositoryImpl(userRepositoryMap);
        final UserServiceInterface userServiceImpl = (UserServiceInterface) getService("User-service");
        userServiceImpl.setUserRepository(userRepository);
        try {
            userServiceImpl.save(initAdmin());
        } catch (IOException e) {
            e.printStackTrace();
        }

        final ProjectEndPoint projectEndPoint = new ProjectEndPoint(projectService,sessionService);
        final TaskEndPoint taskEndPoint = new TaskEndPoint(projectService, taskService, sessionService);
        final UserEndPoint userEndPoint = new UserEndPoint(userServiceImpl,sessionService);
        final DataEndPoint dataEndPoint = new DataEndPoint(dataService,sessionService);
        final SessionEndPoint sessionEndPoint= new SessionEndPoint(sessionService);

        Endpoint.publish("http://localhost:8080/ProjectEndpoint?wsdl", projectEndPoint);
        Endpoint.publish("http://localhost:8080/TaskEndpoint?wsdl", taskEndPoint);
        Endpoint.publish("http://localhost:8080/UserEndpoint?wsdl", userEndPoint);
        Endpoint.publish("http://localhost:8080/DataEndpoint?wsdl", dataEndPoint);
        Endpoint.publish("http://localhost:8080/SessionEndpoint?wsdl",sessionEndPoint);


    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public boolean isValidDate(String inDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(inDate.trim());
        } catch (ParseException pe) {
            return false;
        }
        return true;
    }
    public User initAdmin(){
        User admin = new User();
        admin.setName("admin");
        admin.setPassword(PasswordHashUtil.md5("admin"));
        admin.setUserRoleType(UserRoleType.ADMIN);
        admin.setUserId(UUID.randomUUID().toString());
        return admin;
    }
}
