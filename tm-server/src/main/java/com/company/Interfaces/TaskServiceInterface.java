package com.company.Interfaces;

import com.company.entity.Task;
import com.company.exception.EmptyField;
import com.company.exception.ObjectIsNotFound;
import com.company.util.Bootstrap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public interface TaskServiceInterface {

     String taskCreate(String name, String projectId, Date dateStart, Date dateEnd, String userId, String description) throws Exception;
     Task read (String id);
     void update(Task task);
     void taskRemove(String id) throws ObjectIsNotFound;
     List taskList(String id);
     void taskClear();
     void setTaskRepository(TaskRepoInterface taskRepository);
     void setProjectService(ProjectServiceInterface projectService);
     void setBootstrap(Bootstrap bootstrap);
     void setTasks(List<Task> tasks) throws EmptyField;
     List allTaskList();
     void load() throws IOException, ClassNotFoundException;
     void saveInDb();

}
