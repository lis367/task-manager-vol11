package com.company.Interfaces;

import com.company.entity.Data;
import com.company.exception.EmptyField;
import org.jetbrains.annotations.Nullable;

public interface DataServiceInterface {

    void load(@Nullable Data data) throws EmptyField;
    @Nullable Data save(@Nullable Data data);
    void setServiceLocator(ServiceLocator serviceLocator);
}
