package com.company.Interfaces;

import com.company.entity.Project;
import com.company.exception.ObjectIsNotFound;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public interface ProjectRepoInterface {

    Project findOne(String id) throws ObjectIsNotFound; //read
    List<Project> findAll(String id); //readAll
    List<Project> findAll(); //readAll without ID
    void persist (Project project); // create
    void merge (Project project); //update
    void remove (String string); // delete
    void removeAll(); // deleteAll
    void saveInDB() throws Exception;
    void load() throws IOException, ClassNotFoundException;





}
