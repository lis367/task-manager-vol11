package com.company.Interfaces;

import com.company.entity.Session;
import com.company.entity.User;
import com.company.exception.EmptyField;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface SessionServiceInterface {

    void closeSession(Session session) throws EmptyField;
    List getListSession();
    User getUser();
    Session openSession(User user);
    void setServiceLocator(ServiceLocator serviceLocator);
    void validate(Session session) throws EmptyField;
    public Session sign(@Nullable final Session session);

}
