package com.company.Interfaces;

import com.company.entity.User;
import com.company.exception.EmptyField;
import com.company.exception.ObjectIsNotFound;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public interface UserServiceInterface {

    void registration(String login, String password) throws Exception;
    void update(User user) throws ObjectIsNotFound, IOException;
    String generateMD5 (String password) throws NoSuchAlgorithmException;
    User read(String login, String password);
    void setUsers(List<User> users) throws EmptyField;
    List allUserList();
    void save (User user) throws IOException;
    void load() throws IOException, ClassNotFoundException;
    void remove(User user);
    void setUserRepository(UserRepoInterface userRepositoryImpl);
    void saveInDb();


}
