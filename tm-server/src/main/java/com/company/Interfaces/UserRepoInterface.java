package com.company.Interfaces;

import com.company.entity.User;

import java.io.IOException;
import java.util.List;

public interface UserRepoInterface {

    void persist(User user); //create
    User findOne(String login, String password); //read
    List<User> findAll(); //readAll without ID
    void merge(User user); //update
    void remove(User user); //delete
    void load() throws IOException, ClassNotFoundException;  // Load Users from File
    void save() throws IOException; // Save new User in File


}
